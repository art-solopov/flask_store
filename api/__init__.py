from flask import Blueprint, request, jsonify
from flask.views import MethodView

from app import db
from app.models import Product
from app.forms import ProductForm
from helpers.routes import catch_integrity_error
from . import serializers as ser

api = Blueprint('api', __name__)


@api.route("/products", methods=('GET', 'POST'))
@catch_integrity_error
def products():
    if request.method == 'POST':
        form = ProductForm(request.form)
        if form.validate():
            product = Product()
            form.populate_obj(product)
            db.session.add(product)
            db.session.commit()
            return ser.product(product)
        else:
            return ("Invalid", 422)
    else:
        products = Product.query.order_by('ext_no')
        return jsonify([ser.product(p) for p in products])


class SingleProduct(MethodView):
    def dispatch_request(self, id):
        self.product = Product.query.get_or_404(id)
        return super().dispatch_request()

    def get(self):
        return ser.product(self.product)

    def put(self):
        form = ProductForm(request.form, self.product)
        if form.validate():
            form.populate_obj(self.product)
            db.session.add(self.product)
            db.session.commit()
            return ser.product(self.product)
        else:
            return ('Invalid', 422)

    def delete(self):
        db.session.delete(self.product)
        db.session.commit()
        return {'ok': True}


api.add_url_rule('/products/<id>', view_func=SingleProduct.as_view('product'))
