from flask import url_for

from app.models import Product


def product(product: Product) -> dict:
    return {
        'id': product.id,
        'extNo': product.ext_no,
        'name': product.name,
        'price': float(product.price),
        '_meta': {
            'links': {
                'self': url_for('.product', id=product.id)
            }
        }
    }
