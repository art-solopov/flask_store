from functools import wraps

from sqlalchemy.exc import IntegrityError


def catch_integrity_error(view):
    @wraps(view)
    def new_view(*args, **kwargs):
        try:
            return view(*args, **kwargs)
        except IntegrityError as e:
            print("Caught an integrity error")
            print(e.orig)
            return ('Invalid', 422)
    return new_view
