from wtforms import Form, StringField
from wtforms.fields.html5 import DecimalField
import wtforms.validators as v


class ProductForm(Form):
    ext_no = StringField('External no', [v.InputRequired(), v.Length(min=10, max=24)])
    name = StringField('Name', [v.InputRequired()])
    price = DecimalField('Price', [v.InputRequired(), v.NumberRange(min=0.01)], places=2, render_kw={'step': 0.01})
