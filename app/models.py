from app import db


class Product(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    ext_no = db.Column(db.String(24), unique=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    price = db.Column(db.Numeric(10, 3), nullable=False)
