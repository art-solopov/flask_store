from flask import request, render_template, url_for

from . import app


@app.route("/", methods=('GET', 'POST'))
def hello():
    if request.method == 'POST':
        return str(request.form) + "\n" + str(request.json)
    else:
        urls = [('Management', url_for('management.root'))]
        return render_template("index.html", urls=urls)
