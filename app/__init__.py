from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask('flask_store')
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres:///flask_store'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from api import api
from management import management

app.register_blueprint(api, url_prefix='/api')
app.register_blueprint(management, url_prefix='/management')

from .routes import *
