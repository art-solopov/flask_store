const path = require('path')

module.exports = {
    entry: './frontend/app.js',
    output: {
        path: path.resolve(__dirname, 'static'),
        filename: 'app.js'
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [['@babel/preset-env', {useBuiltIns: 'usage', corejs: 3}]],
                        plugins: [
                            "@babel/plugin-proposal-class-properties",
                            "@babel/plugin-proposal-function-bind"
                        ]
                    }
                }
            }
        ]
    },
    mode: 'development',
    devtool: 'inline-source-map'
}
