from flask import Blueprint, render_template

from app.forms import ProductForm

management = Blueprint('management', __name__,
                       template_folder='templates')


@management.route("/")
def root():
    return render_template("home.html")


@management.route("/products")
def products():
    form = ProductForm()
    return render_template("products.html", form=form)
