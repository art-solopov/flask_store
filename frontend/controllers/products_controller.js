import { Controller } from 'stimulus'
import axios from 'axios'
import { ajax } from 'rxjs/ajax'
import { map, catchError } from 'rxjs/operators'
import { html, render } from 'lit-html'

const productRow = (product) => {
    let {links} = product['_meta']

    return html`
        <tr data-product-id="${product.id}">
            <td>${product.id}</td>
            <td>${product.extNo}</td>
            <td>${product.name}</td>
            <td>${product.price}</td>
            <td>
                <button type="button" data-action="products#editProduct">Edit</button>
                <button type="button" data-action="products#deleteProduct">Delete</button>
            </td>
        </tr>
    `
}

const productsTemplate = (products) => {
    return html`
        ${products.map(productRow)}
    `
}

export default class ProductsController extends Controller {
    static targets = ["table", "tableBody", "form"]

    connect() {
        this._loadProducts()
    }

    processForm(ev) {
        ev.preventDefault()
        let fd = new FormData(this.formTarget)
        console.log(fd)

        let action, method

        if (this.formProduct) {
            action = this.formProduct['_meta'].links.self
            method = 'put'
        } else {
            action = this.formTarget.dataset.createUrl
            method = 'post'
        }

        axios({
            method,
            url: action,
            data: fd
        }).then(res => {
            this._loadProducts()
            this._resetForm()
        })
    }

    editProduct(ev) {
        ev.preventDefault()

        let product = this._findEventProduct(ev)
        if(!product) { return; }

        this.formProduct = product
        this._fillForm(product)
    }

    deleteProduct(ev) {
        ev.preventDefault()

        let product = this._findEventProduct(ev)
        if(!product) { return; }

        axios.delete(product['_meta'].links.self)
    }

    _findEventProduct(ev) {
        let row = ev.target.closest('tr')
        let productId = Number(row.dataset.productId)
        return this.products.find(pr => pr.id === productId)
    }

    _loadProducts() {
        axios.get('/api/products').then(res => {
            this.products = res.data
            this._renderProducts()
        })
    }

    _renderProducts() {
        render(productsTemplate(this.products), this.tableBodyTarget)
    }

    _resetForm() {
        this.formProduct = null
        this.formTarget.reset()
    }

    _fillForm(product) {
        let form = this.formTarget

        form.querySelector(`input#ext_no`).value = product.extNo
        form.querySelector('input#name').value = product.name
        form.querySelector('input#price').value = product.price
    }
}
